<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Say Hello</title>
</head>
<body>
    <form action="">
        Name: <input type="text" name="name"><br>
        Age: <input type="number" name="age">
        <input type="submit" value="Say hello">
    </form>
    <?php
    if (isset($_GET['name'])) {
        $name = $_GET['name'];
        $age = $_GET['age'];
        echo "<p>Hi $name, you are $age y/o</p>";
    }
    ?>
</body>
</html>