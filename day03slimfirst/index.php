<?php

require_once 'vendor/autoload.php';

use Slim\Http\Request;
use Slim\Http\Response;
/*
DB::$dbName = 'day03people';
DB::$user = 'day03people';
DB::$password = 'XA@0lP95]!zE7t8R';
DB::$host = 'localhost';
DB::$port = 3333;
*/
/*
db: cp5003_jessboudreault
user: cp5003_jessboudreault
pass: 6G6jF4t]L63n
*/

if (strpos($_SERVER['HTTP_HOST'], "ipd24.ca") !== false) {
    // hosting on ipd24.com
    DB::$dbName = 'cp5003_jessboudreault';
    DB::$user = 'cp5003_jessboudreault';
    DB::$password = '6G6jF4t]L63n';
} else { // local computer
    DB::$dbName = 'day03people';
    DB::$user = 'day03people';
    DB::$password = 'XA@0lP95]!zE7t8R';
    DB::$port = 3333;
}


// Create and configure Slim app
$config = ['settings' => [
    'addContentLengthHeader' => false,
    'displayErrorDetails' => true
]];
$app = new \Slim\App($config);

// Fetch DI Container
$container = $app->getContainer();

// Register Twig View helper
$container['view'] = function ($c) {
    $view = new \Slim\Views\Twig(dirname(__FILE__) . '/templates', [
        'cache' => dirname(__FILE__) . '/tmplcache',
        'debug' => true, // This line should enable debug mode
    ]);
    //
    $view->getEnvironment()->addGlobal('test1','VALUE');
    // Instantiate and add Slim specific extension
    $router = $c->get('router');
    $uri = \Slim\Http\Uri::createFromEnvironment(new \Slim\Http\Environment($_SERVER));
    $view->addExtension(new \Slim\Views\TwigExtension($router, $uri));
    return $view;
};


// Define app routes
$app->get('/hello/{name}/{age:[0-9]+}', function ($request, $response, $args) {
    $name = $args['name'];
    $age = $args['age'];
    DB::insert('people', ['name' => $name, 'age' => $age]);
    $insertId = DB::insertId();
    return $this->view->render($response, 'hello.html.twig', ['age' => $age, 'name' => $name, 'insertId' => $insertId]);
    //return $response->write("Hello $name, you are $age y/o");
});

// STATE 1: first display the form
$app->get('/addperson', function ($request, $response, $args) {
    return $this->view->render($response, 'addperson.html.twig');
});

//STATE 2&3: receiving form submission
$app->post('/addperson', function (Request $request, Response $response, $args) {
    $name = $request->getParam('name');
    $age = $request->getParam('age');

    // validation
    $errorList = [];
    if(strlen($name) < 2 || strlen($name) > 100) {
        $name ="";
        $errorList[] = "Name must be 2-100 characters long";
    }
    if(filter_var($age, FILTER_VALIDATE_INT) === false || $age < 0 || $age > 150) {
        $age = "";
        $errorList[] = "Age must be a number between 0-150";
    }
    //
    if($errorList) { // STATE 2: errors - show and redisplay the form
        $valuesList = ['name' => $name, 'age' => $age];
        return $this->view->render($response, "addperson.html.twig", ['errorList' => $errorList, 'v' => $valuesList]);
    } else { // STATE 3: success
        DB::insert('people', ['name' => $name, 'age' => $age]);
        return $this->view->render($response, "addperson_success.html.twig");
    }
});

// Run app
$app->run();