<?php
require_once 'vendor/autoload.php';

session_start();

use Slim\Http\Request;
use Slim\Http\Response;

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

// create a log channel
$log = new Logger('main');
$log->pushHandler(new StreamHandler(dirname(__FILE__) . '/logs/everything.log', Logger::DEBUG));
$log->pushHandler(new StreamHandler(dirname(__FILE__) . '/logs/errors.log', Logger::ERROR));

    DB::$dbName = 'quiz1slimshout';
    DB::$user = 'quiz1slimshout';
    DB::$password = '1[Gz]i@7Sc8QWBy6';
    DB::$port = 3333;

DB::$error_handler = 'db_error_handler'; // runs on mysql query errors
DB::$nonsql_error_handler = 'db_error_handler'; // runs on library errors (bad syntax, etc)

function db_error_handler($params) {
    global $log;
    // log first
    $log->error("Database error: " . $params['error']);
    if (isset($params['query'])) {
        $log->error("SQL query: " . $params['query']);
    }
    // redirect
    header("Location: /internalerror");
    die;
}

use Slim\Http\UploadedFile;

// Create and configure Slim app
$config = ['settings' => [
    'addContentLengthHeader' => false,
    'displayErrorDetails' => true
]];
$app = new \Slim\App($config);

// Fetch DI Container
$container = $app->getContainer();

// File upload directory
$container['upload_directory'] = __DIR__ . '/uploads';

// Register Twig View helper
$container['view'] = function ($c) {
    $view = new \Slim\Views\Twig(dirname(__FILE__) . '/templates', [
        'cache' => dirname(__FILE__) . '/tmplcache',
        'debug' => true, // This line should enable debug mode
    ]);
    // Instantiate and add Slim specific extension
    $router = $c->get('router');
    $uri = \Slim\Http\Uri::createFromEnvironment(new \Slim\Http\Environment($_SERVER));
    $view->addExtension(new \Slim\Views\TwigExtension($router, $uri));
    return $view;
};

// All templates will be given userSession variable
$container['view']->getEnvironment()->addGlobal('userSession', $_SESSION['user'] ?? null );

$container['view']->getEnvironment()->addGlobal('flashMessage', getAndClearFlashMessage());

$container['view']->getEnvironment()->addGlobal('picture', convertPicture());


$passwordPepper = 'mmyb7oSAeXG9DTz2uFqu';

//********************************** URLS ********************************* */

$app->get('/internalerror', function ($request, $response, $args) {
    return $this->view->render($response, 'internalerror.html.twig');
});

// *************** INDEX ******************
// for index.html.twig:
    $app->get('/', function (Request $request, Response $response, $args) {
        return $this->view->render($response, 'index.html.twig');
    });

// ****************** REGISTER **************
// STATE 1: first display the form
$app->get('/register', function (Request $request, Response $response, $args) {
    return $this->view->render($response, 'register.html.twig', ['userSession' => $_SESSION['user'] ?? null]);
});

//STATE 2&3: receiving form submission
$app->post('/register', function (Request $request, Response $response, $args) {
    $name = $request->getParam('name');
    $pass1 = $request->getParam('pass1');
    $pass2 = $request->getParam('pass2');

    // ***** VALIDATION *****
    $errorList = [];

    // verify user name
    $result = verifyUserName($name);
    if ($result !== TRUE) { $errorList[] = $result; }

    // verify password
    $result = verifyPasswordQuality($pass1, $pass2);
    if ($result !== TRUE) { $errorList[] = $result; };

    // verify image
    $hasPhoto = false;
    $uploadedImage = $request->getUploadedFiles()['image'];
    if ($uploadedImage->getError() != UPLOAD_ERR_NO_FILE) { // was anything uploaded?
        // print_r($uploadedImage->getError());
        $hasPhoto = true;
        $result = verifyUploadedPhoto($uploadedImage);
        if ($result !== TRUE) {
            $errorList[] = $result;
        } 
    }

    if ($errorList) {
        return $this->view->render($response, 'register.html.twig',
                ['errorList' => $errorList, 'v' => ['name' => $name]]);
    } else {
        if ($hasPhoto) {
            $directory = $this->get('upload_directory');
            $uploadedImagePath = moveUploadedFile($directory, $uploadedImage);
            if ($uploadedImagePath == FALSE) {
                return $response->withRedirect("/internalerror", 301);
            }
        }
        //
        global $passwordPepper;
        $pwdPeppered = hash_hmac("sha256", $pass1, $passwordPepper);
        $pwdHashed = password_hash($pwdPeppered, PASSWORD_DEFAULT);
        DB::insert('users', ['username' => $name, 'password' => $pwdHashed, 'imagePath' => $uploadedImagePath]);
        setFlashMessage("You've been registered. Please login now");
        return $response->withRedirect('/login');
    }

});

// ************** LOGIN *******************
// STATE 1: first display
$app->get('/login[/{params:.*}]', function (Request $request,Response $response, $args) {
    return $this->view->render($response, 'login.html.twig');
});

// STATE 2&3: receiving submission
$app->post('/login[/{params:.*}]', function ($request, $response, $args) use ($log) {
    $username = $request->getParam('username');
    $password = $request->getParam('password');
    //
    $record = DB::queryFirstRow("SELECT id,username,password,imagePath FROM users WHERE username=%s", $username);
    $loginSuccess = false;
    if ($record) {
        global $passwordPepper;
        $pwdPeppered = hash_hmac("sha256", $password, $passwordPepper);
        $pwdHashed = $record['password'];
        if (password_verify($pwdPeppered, $pwdHashed)) {
            $loginSuccess = true;
        }
        else if ($record['password'] == $password) {
            $loginSuccess = true;
        }
    }
    //
    if (!$loginSuccess) {
        $log->info(sprintf("Login failed for username %s from %s", $username, $_SERVER['REMOTE_ADDR']));
        return $this->view->render($response, 'login.html.twig', [ 'error' => true ]);
    } else {
        unset($record['password']); // for security reasons remove password from session
        $_SESSION['user'] = $record; // remember user logged in
        $log->debug(sprintf("Login successful for username %s, uid=%d, from %s", $username, $record['id'], $_SERVER['REMOTE_ADDR']));
        return $this->view->render($response, 'index.html.twig', ['userSession' => $_SESSION['user'] ] );
        setFlashMessage("You've been logged in");
        if($args['params']) { // if there is link to redirect, then transfer user after login to this page
            return $response->withRedirect("/".$args['params']);
        }
        return $response->withRedirect("/");

      }
});

// ******************* LOGOUT *********************
$app->get('/logout', function ($request, $response, $args) use ($log) {
    $log->debug(sprintf("Logout successful for uid=%d, from %s", @$_SESSION['user']['id'], $_SERVER['REMOTE_ADDR']));
    unset($_SESSION['user']);
    return $this->view->render($response, 'logout.html.twig', ['userSession' => null ]);
});

// *********************** SHOUTS ADD ****************
// STATE 1: first display
$app->get('/shoutsadd', function (Request $request,Response $response, $args) {
    if(!isset($_SESSION['user'])) {
        $response = $response->withStatus(403);
        return $this->view->render($response, 'forbidden.html.twig');
    }
    $username = $_SESSION['user']['username'];
    $record = DB::queryFirstRow("SELECT username FROM users WHERE username=%s", $username);
    return $this->view->render($response, 'shoutsadd.html.twig', ['user' => $record]);
});

//STATE 2&3: receiving form submission
$app->post('/shoutsadd', function (Request $request, Response $response, $args) {
    if(!isset($_SESSION['user'])) {
        $response = $response->withStatus(403);
        return $this->view->render($response, 'forbidden.html.twig');
    }
    $message = $request->getparam('message');
    $message = strip_tags($message, "<p><ul><li><em><strong><i><b><ol><h3><h4><h5><span>");

    //Verify message
    $result = verifyMessage($message);
    if ($result !== TRUE) { $errorList[] = $result; }

    if ($errorList) {
        return $this->view->render($response, 'shoutsadd.html.twig',
                ['errorList' => $errorList, 'v' => ['message' => $message]]);
    } else {
        $authorId = $_SESSION['user']['id'];
        DB::insert('shouts', ['authorId' => $authorId, 'message' => $message]); //NOT WORKING, I DON'T UNDERSTAND WHY
        setFlashMessage("Message added to SHOUTS List");
        return $response->withRedirect('/shoutslist');
    }
});


// ******************** SHOUTS LIST *******************

$app->get('/shoutslist', function(Request $request, Response $response, $args) {
    $records = DB::query("SELECT u.username,u.imagePath,s.id,s.authorId,s.message FROM shouts as s inner join users as u on s.authorId=u.id"); // list of shouts from the database
    $userDropdown = DB::query("SELECT username FROM users ORDER BY username"); // for dropdown menus
    return $this->view->render($response, 'shoutslist.html.twig', ['shoutslist' => $records, 'users' => $userDropdown]);
});


// *********************** FUNCTIONS **********************

function convertPicture() {
    $picture = null;
    $convertedPicture = ($_SESSION['user']['imagePath'] ??  null);
    if(!empty($convertedPicture)) {
        $picture = base64_encode($convertedPicture);
    }
    return $picture;
}

function verifyUserName($name) {
    if (preg_match('/^[a-zA-Z0-9\ \\._\'"-]{4,50}$/', $name) != 1) { // no match
        return "Name must be 4-50 characters long and consist of letters, digits, "
            . "spaces, dots, underscores, apostrophies, or minus sign.";
    }
    return TRUE;
}

function verifyPasswordQuality($pass1, $pass2) {
    if ($pass1 != $pass2) {
        return "Passwords do not match";
    } else {
        if ((strlen($pass1) < 6) || (strlen($pass1) > 100)
                || (preg_match("/[A-Z]/", $pass1) == FALSE )
                || (preg_match("/[a-z]/", $pass1) == FALSE )
                || (preg_match("/[0-9]/", $pass1) == FALSE )) {
            return "Password must be 6-100 characters long, "
                . "with at least one uppercase, one lowercase, and one digit in it";
        }
    }
    return TRUE;
}

function verifyUploadedPhoto($photo, &$mime = null) {
    if ($photo->getError() != 0) {
        return "Error uploading photo " . $photo->getError();
    }
    $info = getimagesize($photo->file);
    if (!$info) {
        return "File is not an image";
    }
    if ($info[0] < 50 || $info[0] > 500 || $info[1] < 50 || $info[1] > 500) {
        return "Width and height must be within 50-500 pixels range";
    }
    $ext = "";
    switch ($info['mime']) {
        case 'image/jpeg': $ext = "jpg"; break;
        case 'image/gif': $ext = "gif"; break;
        case 'image/png': $ext = "png"; break;
        default:
            return "Only JPG, GIF and PNG file types are allowed";
    } 
    if (!is_null($mime)) {
        $mime = $info['mime'];
    }
    return TRUE;
}

function moveUploadedFile($directory, UploadedFile $uploadedFile) {
    // Avoid a serious security flaw - user must not be ablet o upload .php file and exploit our server
    $extension = strtolower(pathinfo($uploadedFile->getClientFilename(), PATHINFO_EXTENSION));
    if (!in_array($extension, ['jpg', 'jpeg', 'gif', 'png'])) {
        return FALSE;
    }
    $basename = ($_SESSION['user']['username']);
    $filename = sprintf('%s.%0.8s', $basename, $extension);
    try {
        $uploadedFile->moveTo($directory . DIRECTORY_SEPARATOR . $filename); // FIXME catch exception on failure
    } catch (Exception $e) {
        // TODO: log the error message
        return FALSE;
    }
    return $filename;
}

function setFlashMessage($message) {
    $_SESSION['flashMessage'] = $message;
}

function getAndClearFlashMessage() {
    if (isset($_SESSION['flashMessage'])) {
        $message = $_SESSION['flashMessage'];
        unset($_SESSION['flashMessage']);
        return $message;
    }
    return "";
}

function verifyMessage($message) {
    if(strlen($message) < 1 || strlen($message) > 100) {
        return "Messages can only be between 1-100 characters long.";
    }
    return true;
}


    $app->run();
?>